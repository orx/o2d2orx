package net.volcanomobile.o2d2orx.model;

import java.util.Locale;

/**
 * Created by loki on 1/23/16.
 */
public class Scene {

    private static final String SCENE_SECTION = "[%1$s]\n";

    private String sceneName;
    private Composite composite;
    private boolean lightSystemEnabled;
    private float[] ambientColor;
    private PhysicsProperties physicsPropertiesVO;

    public String getName() {
        return sceneName;
    }

    public void output(float pixelToWorld) {
        if(composite != null) {
            composite.outputChildren(sceneName, true, pixelToWorld);

            System.out.format(Locale.US, SCENE_SECTION, sceneName);
            composite.outputChildList();
            composite.outputLayers();
            System.out.println();
        }
    }

    public static class PhysicsProperties {

        private boolean enabled;
        private float gravityX, gravityY;
    }
}
