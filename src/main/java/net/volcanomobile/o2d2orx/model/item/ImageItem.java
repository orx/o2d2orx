package net.volcanomobile.o2d2orx.model.item;

import java.util.Locale;

/**
 * Created by loki on 1/24/16.
 */
public class ImageItem extends BaseItem {

    private static final String OBJECT_GRAPHIC  = "Graphic       = Graphic_%1$s\n";
    private static final String OBJECT_COLOR    = "Color         = (%1$d, %2$d, %3d)\n";
    private static final String OBJECT_ALPHA    = "Alpha         = %1$.4f\n";

    private String imageName;
    private boolean isRepeat;
    private boolean isPolygon;

    @Override
    public void output(String name, float pixelToWorld) {
        super.output(name, pixelToWorld);

        System.out.format(Locale.US, OBJECT_POSITION, x + originX, - (y + originY), 1f - (0.01f * zIndex));
        System.out.format(Locale.US, OBJECT_SCALE, scaleX / pixelToWorld, scaleY / pixelToWorld);
        System.out.format(Locale.US, OBJECT_GRAPHIC, imageName);
        System.out.format(Locale.US, OBJECT_COLOR, (int) tint[0] * 255, (int) tint[1] * 255, (int) tint[2] * 255);
        System.out.format(Locale.US, OBJECT_ALPHA, tint[3]);
    }
}
