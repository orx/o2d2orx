package net.volcanomobile.o2d2orx.model.item;

import java.util.List;
import java.util.Locale;

/**
 * Created by loki on 1/24/16.
 */
public class BaseItem {

    private static final String OBJECT_SECTION    = "[%1$s]\n";
    protected static final String OBJECT_POSITION = "Position      = (%1$.4f, %2$.4f, %3$.4f)\n";
    protected static final String OBJECT_SCALE    = "Scale         = (%1$.4f, %2$.4f, 0)\n";
    private static final String OBJECT_ROTATION   = "Rotation      = %1$.4f\n";
    private static final String OBJECT_GROUP      = "Group         = %1$s\n";

    private int uniqueId;
    private String itemIdentifier;
    private List<String> tags;
    private String customVars;
    protected double x, y;
    protected double scaleX = 1f, scaleY = 1f;
    protected double originX, originY;
    private double rotation = 0;
    protected int zIndex = 0;
    private String layerName;
    protected float[] tint = {1f, 1f, 1f, 1f};
    private String shaderName;
    private Physics physics;

    public void output(String name, float pixelToWorld) {
        System.out.format(Locale.US, OBJECT_SECTION, name);
        if(rotation != 0) {
            System.out.format(Locale.US, OBJECT_ROTATION, - rotation);
        }
    }

    public final void outputGroup() {
        System.out.format(Locale.US, OBJECT_GROUP, layerName);
    }

    public static class Physics {

        float mass;
        boolean allowSleep;
        boolean awake;
        float density;
        float friction;
        float restitution;
    }
}
