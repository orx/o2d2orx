package net.volcanomobile.o2d2orx.model.item;

/**
 * Created by loki on 1/24/16.
 */
public class LabelItem extends BaseItem {

    private String text;
    private String style;
    private float size;
    private int align; // cf https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/utils/Align.java
    private float width, height;
    private boolean multiline;
}
