package net.volcanomobile.o2d2orx.model.item;

import net.volcanomobile.o2d2orx.model.Composite;

import java.util.Locale;

/**
 * Created by loki on 1/24/16.
 */
public class CompositeItem extends BaseItem {

    private Composite composite;
    private float scissorX, scissorY, scissorWidth, scissorHeight;
    private float width, height;

    @Override
    public void output(String name, float pixelToWorld) {
        if(composite != null) {
            composite.outputChildren(name, false, pixelToWorld);
        }

        super.output(name, pixelToWorld);
        System.out.format(Locale.US, OBJECT_POSITION, x , - y, 1f - (0.01f * zIndex));
        System.out.format(Locale.US, OBJECT_SCALE, scaleX, scaleY);
        composite.outputChildList();
    }
}
