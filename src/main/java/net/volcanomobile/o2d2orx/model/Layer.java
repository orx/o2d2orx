package net.volcanomobile.o2d2orx.model;

/**
 * Created by loki on 1/24/16.
 */
public class Layer {

    private String layerName;
    private boolean isLocked;
    private boolean isVisible;

    public String getLayerName() {
        return layerName;
    }

    public boolean isVisible() {
        return isVisible;
    }
}
