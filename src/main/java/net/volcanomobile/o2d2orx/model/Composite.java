package net.volcanomobile.o2d2orx.model;

import net.volcanomobile.o2d2orx.model.item.CompositeItem;
import net.volcanomobile.o2d2orx.model.item.ImageItem;
import net.volcanomobile.o2d2orx.model.item.LabelItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by loki on 1/24/16.
 */
public class Composite {

    private static final String CHILD_LIST = "ChildList     = %1$s\n";
    private static final String LAYER_LIST = "LayerList     = %1$s\n";

    private List<ImageItem> sImages;
    private List<LabelItem> sLabels;
    private List<CompositeItem> sComposites;
    private List<Layer> layers;

    public void outputChildren(String baseName, boolean outputGroup, float pixelToWorld) {
        if(sImages != null) {
            for(ImageItem imageItem : sImages) {
                String childName = generateChildName(baseName);
                imageItem.output(childName, pixelToWorld);
                if(outputGroup) {
                    imageItem.outputGroup();
                }
                System.out.println();
            }
        }

        if(sComposites != null) {
            for(CompositeItem compositeItem : sComposites) {
                String childName = generateChildName(baseName);
                compositeItem.output(childName, pixelToWorld);
                if(outputGroup) {
                    compositeItem.outputGroup();
                }
                System.out.println();
            }
        }
    }

    public void outputLayers() {
        if(layers != null) {
            Collections.reverse(layers);
            List<String> visibleLayers = new ArrayList<>();
            for(Layer layer : layers) {
                if(layer.isVisible()) {
                    visibleLayers.add(layer.getLayerName());
                }
            }
            System.out.format(Locale.US, LAYER_LIST, String.join(" # ", visibleLayers));
        }
    }

    public void outputChildList() {
        System.out.format(Locale.US, CHILD_LIST, String.join(" # ", childsName));
    }

    private int childIndex = 0;
    private List<String> childsName = new ArrayList<>();

    public String generateChildName(String baseName) {
        String childName = String.format(Locale.US, "%s_Child%d", baseName, childIndex++);
        childsName.add(childName);
        return childName;
    }
}
