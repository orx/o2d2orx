package net.volcanomobile.o2d2orx.model;

import java.util.List;
import java.util.Locale;

/**
 * Created by loki on 1/23/16.
 */
public class Project {

    private static final String DISPLAY_SECTION = "[Display]\n" +
            "ScreenWidth   = %1$d\n" +
            "ScreenHeight  = %2$d\n\n";

    private static final String MAIN_VIEWPORT_SECTION = "[MainViewport]\n" +
            "Camera = MainCamera\n\n";

    private static final String MAIN_CAMERA_SECTION = "[MainCamera]\n" +
            "FrustumNear   = 0.0\n" +
            "FrustumFar    = 20.0\n" +
            "Position      = (0.0, 0.0, -1.0)\n" +
            "FrustumWidth  = %1$.2f\n" +
            "FrustumHeight = %2$.2f\n\n";

    private float pixelToWorld = 1f;
    private Resolution originalResolution;
    private List<Scene> scenes;

    public List<Scene> getScenes() {
        return scenes;
    }

    public Resolution getOriginalResolution() {
        return originalResolution;
    }

    public float getPixelToWorld() {
        return pixelToWorld;
    }

    public void output() {
        double frustumWidth, frustumHeight;
        frustumWidth = originalResolution.getWidth() / pixelToWorld;
        frustumHeight = originalResolution.getHeight() / pixelToWorld;

        System.out.format(Locale.US, DISPLAY_SECTION, (int)originalResolution.getWidth(), (int)originalResolution.getHeight());
        System.out.print(MAIN_VIEWPORT_SECTION);
        System.out.format(Locale.US, MAIN_CAMERA_SECTION, frustumWidth, frustumHeight);
    }

    public static class Resolution {
        String name;
        float width;
        float height;

        public float getWidth() {
            return width;
        }

        public float getHeight() {
            return height;
        }

        public String getName() {
            return name;
        }
    }
}
