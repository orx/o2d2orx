package net.volcanomobile.o2d2orx;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.google.gson.Gson;
import net.volcanomobile.o2d2orx.model.Project;
import net.volcanomobile.o2d2orx.model.Scene;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by loki on 1/23/16.
 */
public class Main {

    private static final String DEFAULT_PROJECT_FILENAME = "project.dt";
    private static final String SCENE_FILENAME = "./scenes/%s.dt";
    private static final String ATLAS_FILENAME = "./%s/pack.atlas";

    private static final String GRAPHIC_SECTION = "[Graphic_%1$s]\n" +
            "Smoothing     = true\n" +
            "Texture       = %2$s\n" +
            "Pivot         = center\n" +
            "TextureOrigin = (%3$d, %4$d, 0)\n" +
            "TextureSize   = (%5$d, %6$d, 0)\n\n";

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Gson gson = new Gson();
        FileReader reader;

        Project project = null;

        try {
            // parse main project file
            reader = new FileReader(DEFAULT_PROJECT_FILENAME);
            project = gson.fromJson(reader, Project.class);
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }

        if(project != null) {

            project.output();

            FileHandle packFile = new FileHandle(String.format(Locale.US, ATLAS_FILENAME, project.getOriginalResolution().getName()));
            TextureAtlas.TextureAtlasData atlasData = new TextureAtlas.TextureAtlasData(packFile, packFile.parent(), false);

            for(TextureAtlas.TextureAtlasData.Region region : atlasData.getRegions()) {
                System.out.format(Locale.US, GRAPHIC_SECTION,
                        region.name,
                        region.page.textureFile.name(),
                        region.left, region.top,
                        region.width, region.height);
            }

            List<Scene> scenes = project.getScenes();
            if(scenes != null) {
                // parse scenes files
                for(Scene scene : scenes) {
                    try {
                        reader = new FileReader(String.format(Locale.US, SCENE_FILENAME, scene.getName()));
                        Scene completeScene = gson.fromJson(reader, Scene.class);
                        reader.close();

                        completeScene.output(project.getPixelToWorld());
                    } catch (IOException e) {
                        logger.error(e);
                    }
                }
            }
        }
    }
}
